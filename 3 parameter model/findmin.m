function obj = findmin(theta,Time,dCC,ninit0,A0,W_initial,Fraction_t_int,MeaninBandt,Bandwith,dCCL_H2O,NewdisFractionend,N_end,Cum_total,vec,vec_CSD,mseed)
theta

[n,t,dCt_model,mt_model,mt_t_model,B,G,Final_fraction_model,d43_model,d10_model,d50_model,d90_model,ninit1,dCt_model_H2O,M_4,M_3,w_alpha,w_beta,dC_alpha,Final_fraction_model_cum]  = Mass_balance_FVM(theta,Time,ninit0,A0,W_initial,MeaninBandt,Bandwith,mseed);

% objective function SSE
obj = sum((dCCL_H2O-dCt_model_H2O(vec)).^2)/(mean(dCCL_H2O).^2)...
    +sum(sum((Cum_total(1:end-1,2:end)-Final_fraction_model_cum(vec_CSD(2:end),1:end)').^2)./(mean(Cum_total(1:end-1,2:end)).^2))



end