function [n,t,dCt_model,mt_model,mt_t_model,B,G,Final_fraction_model,d43,d10,d50,d90,ninit1,dCt_model_H2O,M_4,M_3,w_alpha,w_beta,dC_alpha,Final_fraction_model_cum,M_2,M_1]  = Mass_balance_FVM(theta,Time,ninit0,A0,W_initial,MeaninBandt,Bandwith,mseed)


global  M_lactose M_H2O M_lactose_H2O rho kv ks w_eq m_sol UpperBoundN datAll    

%% Solving for N as function of time
%creating zero matrix
dCt_model = zeros(1,length(Time))';
dCt_model_H2O = zeros(1,length(Time))';
dC_alpha = zeros(1,length(Time))';
w_alpha = zeros(1,length(Time))';
d10 = zeros(1,length(Time))';
d50 = zeros(1,length(Time))';
d90 = zeros(1,length(Time))';
d43 = zeros(1,length(Time))';
M_3= zeros(1,length(Time))';
M_4 = zeros(1,length(Time))';
B = zeros(1,length(Time))';
mt_model = zeros(1,length(Time))'; 
m_con_water = zeros(1,length(Time))';
m_con_lactose = zeros(1,length(Time))';
m_con_total = zeros(1,length(Time))';
w_con = zeros(1,length(Time))';
ninit1 = zeros(length(Time)-1,length(ninit0));
A  = zeros(length(Time),length(MeaninBandt));
Final_fraction_model = zeros(length(Time),length(MeaninBandt));
mt_t_model= zeros(length(Time),length(MeaninBandt));
G =zeros(length(Time),1);

% Initial values
w_eq_H2O = w_eq/(1-w_eq);
dCt_model(1) = W_initial-w_eq;
dCt_model_H2O(1) = W_initial/(1-W_initial)-w_eq_H2O;
m_con_water(1) = m_sol*(1-W_initial);
m_con_lactose(1)= m_sol*W_initial;
m_con_total(1) = m_sol;
w_con(1) = W_initial;
ninit1(1,:) = ninit0';
mt_t_model(1,:)= ninit0'.*(rho.*kv*(MeaninBandt(1:end)'.^3));
mt_model (1) = sum(mt_t_model(1,:));
A (1,:) = A0;%
Final_fraction_model (1,:)=ninit0'.*(kv.*(MeaninBandt(1:end)'.^3))./sum(ninit0'.*(kv.*(MeaninBandt(1:end)'.^3)));
Final_fraction_model_cum(1,:) = cumsum(Final_fraction_model (1,:));
if sum(ninit0)>0
    d10(1) = UpperBoundN(find(Final_fraction_model_cum(1,:)>0.1,1));
    d50(1) = UpperBoundN(find(Final_fraction_model_cum(1,:)>0.5,1));
    d90(1) = UpperBoundN(find(Final_fraction_model_cum(1,:)>0.9,1));
    Final_fraction_model (1,:)=ninit0'.*(kv.*(MeaninBandt(1:end)'.^3))./sum(ninit0'.*(kv.*(MeaninBandt(1:end)'.^3)));
    Final_fraction_model_cum(1,:) = cumsum(Final_fraction_model (1,:));
else 
    d10(1) = 0;
    d50(1) = 0;
    d90(1) =0;
    Final_fraction_model (1,:) = ninit0';
    Final_fraction_model_cum(1,:) = cumsum(Final_fraction_model (1,:));
end

M_4 = sum((ninit0/sum(ninit0)).*(MeaninBandt.^4));
M_3 = sum((ninit0/sum(ninit0)).*(MeaninBandt.^3));
d43(1) = M_4/M_3;
%% inital values for alpha/beta alctsoe concentration 
Km = -0.002286*(-1)+1.6371;
w_alpha(1)= ((1/(1+Km))*(dCt_model_H2O(1)+w_eq_H2O));
w_beta(1)= ((Km)*w_alpha(1)); %dCt_model_H2O+w_eq_H2O
F = exp((-2374.6/271)+4.5683);
alpha_eq  = w_eq_H2O/(1+Km);
alpha_s = alpha_eq-F*(w_beta(1)-Km*alpha_eq);
dC_alpha(1)= w_alpha(1)-alpha_s;


for i=1:(length(Time)-1)
    % Growth rate, length dependency apears in FVM_integration
    Gtrial1_1 = 10^(-theta(1)).*((dC_alpha(i)).^2.2);
    G(i) = Gtrial1_1;
    %Nucleation rate
    B0 = 10^theta(3).*((dC_alpha(i)).^1.5)*(mt_model(i)/m_con_water(i));
    B(i,:) =B0';
    %Time span to integrate over
    tspan = [0 (Time(i+1)-Time(i))];
    options1 = odeset('NonNegative',1);
    [t,n] = ode45(@(t,n)FVM_integration(t,n,B0,Gtrial1_1,Bandwith(1:end)*10^6,datAll,1,theta(2)),tspan,ninit1(i,:)./(Bandwith'*10^6),options1);
    n = n.*(Bandwith'*10^6);
    %%Determening alpha and beta fraction
    [row, colom] = size(n);
    mt_alpha= (((sum(((n(:,1:end)).*(rho.*kv*(((MeaninBandt(1:end)'.^3)+zeros(row,colom).^3))))')-mseed)/M_lactose_H2O*M_lactose)')./m_con_water (i);
    dmt_alpha = zeros(length(mt_alpha),1);
    dmt_alpha(2:end) = diff(mt_alpha)./diff(t);
    dmt_alpha(1) =  dmt_alpha(2);
    w0 = [w_alpha(i);w_beta(i)];
    t2= t(1:end);
    [t1,w_alpha_Beta] =  ode45(@(t1,w_alpha_Beta)mutarotation(t1,w_alpha_Beta,t2,dmt_alpha,Km),tspan,w0,options1);     
    w_alpha(i+1) =w_alpha_Beta(end,1);
    w_beta(i+1) =w_alpha_Beta(end,2); 
    alpha_s= alpha_eq-F*(w_beta(i+1)-Km*alpha_eq);
    dC_alpha(i+1) = w_alpha(i+1)-alpha_s;
    if dC_alpha(i+1) < 0
       dC_alpha(i+1) = 0;
    end
    %% New mass balances 
    q=0;
    ninit1(i+1,:) = n(end,:)'+q';
    A(i+1,:) = ninit1(i+1,:).*(ks.*(MeaninBandt(1:end)'.^2));
    Final_fraction_model(i+1,:)=ninit1(i+1,:).*(kv.*(MeaninBandt(1:end)'.^3))./sum(ninit1(i+1,:).*(kv.*(MeaninBandt(1:end)'.^3)));
    Final_fraction_model_cum(i+1,:) = cumsum(Final_fraction_model(i+1,:));
    % setting up new mass balance
    mt_t_model(i+1,:) =((n(length(n(:,1)),1:end)).*(rho.*kv*(MeaninBandt(1:end)'.^3)));
    mt_model(i+1) =sum((n(length(n(:,1)),1:end)).*(rho.*kv*(MeaninBandt(1:end)'.^3)));
    %  water and lactose amount in the solution
    m_con_water (i+1) = m_sol*(1-w_con(1)) - (mt_model(i+1)-mseed)/M_lactose_H2O*M_H2O;
    m_con_lactose(i+1) = m_sol*w_con(1) - (mt_model(i+1)-mseed)/M_lactose_H2O*M_lactose;
    m_con_total (i+1) =  m_con_water (i+1) + m_con_lactose (i+1);
    w_con(i+1) = m_con_lactose(i+1)/m_con_total(i+1);
    if w_con(i+1)> 10.75/100
        dCt_model(i+1) = w_con(i+1)-10.75/100;
        dCt_model_H2O(i+1) = w_con(i+1)/(1-w_con(i+1))-w_eq_H2O;
    else
        dCt_model(i+1) = 10^-10;
        dCt_model_H2O(i+1) =10^-10;
    end
    % Determening new percentiles, moments and average sizes
    d10(i+1,:) =1;      %not used in calculations
    d50(i+1,:) = 1;
    d90(i+1,:) =1;
    M_4(i+1) = sum((n(end,:)').*(MeaninBandt.^4));
    M_3(i+1) = sum((n(end,:)').*(MeaninBandt.^3));
    M_2(i+1) = sum((n(end,:)').*(MeaninBandt.^2));
    M_1(i+1) = sum((n(end,:)').*(MeaninBandt.^1)); 
    d43(i+1) = M_4(i+1)/M_3(i+1);

    
end

end