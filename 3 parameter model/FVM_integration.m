function dndt  = FVM_integration(t,n,B0,G,Bandwith,datAll,theta3,theta4)
% Use of runge kutta integration for determining the number distribution
% over time, see paper Qamar et al.: A comparative study og high resolution schemes for solving population balances in crystallization   
% With n=[n0,n1,n2,n3,n4.....ni] 
% Nucleation rate Beta
% Growth rate Gamma
dndt = zeros(length(Bandwith),1); 


N=length(Bandwith);
% growth rate
G = (G*10^6).*(1+datAll*10^6).^(theta4); 
n0 =  B0./G(1);


for i =1:N
    if i==1
        dndt(i)=(1/Bandwith(i))*(-G(i+1)*n(i)+n0*G(i));
    elseif i==N
        dndt(i)=(1/Bandwith(i-1))*(Bandwith(i-1)./Bandwith(i))*(G(i)*n(i-1));
    else
        dndt(i)=(1/Bandwith(i-1))*(Bandwith(i-1)./Bandwith(i))*(G(i)*n(i-1))-(1/Bandwith(i))*G(i+1)*n(i);
    end
end




end

