function dw_alpha_Betadt = mutarotation(t,w_alpha_Beta,t2,dmt_alpha,Km)

%Mutarotation at -1C including lactose crystallization
% dα/dt = k2*β - k1*α - change of lactose mass over time           
% dβ/dt = k1*α - k2*β 
% dcda6 = [dα/dt, dβ/dt]

f = interp1(t2,dmt_alpha,t);

k_alpha = 0.001311*60;         %Value interpolated from Haase (hour-1) (1966)
k1 = (1/(1+1/Km))*k_alpha;
k2 = (1/Km)*k1;


dw_alpha_Betadt = [(-k1*w_alpha_Beta(1)+k2*w_alpha_Beta(2)-f) ; (k1*w_alpha_Beta(1)-k2*w_alpha_Beta(2))];
end
